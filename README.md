<p align="center"><img src="./art/banner.webp" alt="Message Frontend"></p>

# Message Frontend

Message is an application where users can register an account and post messages using JSON Web Token authentication. It
is also possible to subscribe to other users in order to get their messages on your feed.

Message consists of two different projects:

- A backend made with Laravel.
- A frontend made with VueJS.

## Installation

In order to use the application you'll have to follow the steps below.

### Create the .env file

Create your own **.env** file for configuration. You can do this by copying the example file.

```shell
cp .env.example .env
```

### Set environment variables

Set the variable `VITE_API_URL` and `VITE_API_VERSION` to the correct values.

### Install node modules

```shell
npm install
```

## Usage

There are a few npm scripts available in order to use the application.

### Running a development server

The following command enables compilation and hot-reloads for development.

```shell
npm run dev
```

### Production

The following command compiles and minifies the code for production.

```shell
npm run build
```

## License

This project is released under the [MIT](/LICENSE.md) license.
