import { createApp } from 'vue'
import { createPinia } from 'pinia'
import router from '@/router'
import { initialize } from '@/auth'
import App from '@/App.vue'
import '@/app.css'

const pinia = createPinia()

createApp(App)
    .use(router)
    .use(pinia)
    .mount('#app')

initialize()
