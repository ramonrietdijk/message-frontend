import api from '@/api'
import router from '@/router'
import { useAuthStore } from '@/stores/auth'

export function login(user, token) {
    setUser(user)
    setToken(token)
    router.push({ name: 'dashboard' })
}

export function logout() {
    setUser(null)
    setToken(null)
    router.push({ name: 'auth.login' })
}

export function setUser(user) {
    const authStore = useAuthStore()

    authStore.setUser(user)
}

export function setToken(token) {
    const authStore = useAuthStore()

    if (token === null) {
        localStorage.removeItem('token')
    } else {
        localStorage.setItem('token', token)
    }

    authStore.setToken(token)
}

export function setStatus(status) {
    const authStore = useAuthStore()

    authStore.setStatus(status)
}

export function initialize() {
    setInterval(() => checkToken(), 1e4)

    const token = localStorage.getItem('token')

    if (token) {
        setToken(token)

        api
            .get('auth/user')
            .then(response => setUser(response.data.user))
            .catch(function (error) {
                if (error.response.status === 401) {
                    logout()
                }
            })
            .then(() => setStatus(true))

    } else {
        setStatus(true)
    }
}

export function checkToken() {
    const { token } = useAuthStore()

    if (!token) {
        return
    }

    const left = timeLeft(token)

    if (left < 60) {
        refreshToken()
    }
}

export function refreshToken() {
    api
        .post('auth/refresh')
        .then(response => setToken(response.data.token))
        .catch(function (error) {
            if (error.response.status === 401) {
                logout()
            }
        })
}

export function timeLeft(token) {
    const parts = token.match(/([^.]+)/g)
    const payload = JSON.parse(atob(parts[1]))
    const now = Math.floor(Date.now() / 1000)

    return payload.exp - now
}

export function hasValidToken() {
    const { token } = useAuthStore()

    if (token === null) {
        return false
    }

    return timeLeft(token) > 0
}
