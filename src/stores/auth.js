import { defineStore } from 'pinia'

export const useAuthStore = defineStore('auth', {
    state: () => ({
        user: null,
        token: null,
        status: false
    }),
    actions: {
        setUser: function (user) {
            this.user = user
        },
        setToken: function (token) {
            this.token = token
        },
        setStatus: function (status) {
            this.status = status
        }
    }
})
