import axios from 'axios'
import { useAuthStore } from '@/stores/auth'

const URL = import.meta.env.VITE_API_URL
const VERSION = import.meta.env.VITE_API_VERSION

const api = axios.create({
    baseURL: `${URL}/${VERSION}`
})

api.interceptors.request.use(async function (config) {
    const { token } = useAuthStore()

    if (token) {
        config.headers.Authorization = `Bearer ${token}`
    }

    return config

})

export default api
