import { createRouter, createWebHistory } from 'vue-router'
import { hasValidToken } from '@/auth'
import DashboardPage from '@/pages/DashboardPage.vue'
import LoginPage from '@/pages/auth/LoginPage.vue'
import RegisterPage from '@/pages/auth/RegisterPage.vue'
import GeneralPage from '@/pages/setting/GeneralPage.vue'
import PicturePage from '@/pages/setting/PicturePage.vue'
import EmailPage from '@/pages/setting/EmailPage.vue'
import PasswordPage from '@/pages/setting/PasswordPage.vue'
import PrivacyPage from '@/pages/setting/PrivacyPage.vue'
import ProfilePage from '@/pages/ProfilePage.vue'

const routes = [
    {
        path: '/',
        component: DashboardPage,
        name: 'dashboard',
        meta: {
            auth: true
        }
    },
    {
        path: '/auth/login',
        component: LoginPage,
        name: 'auth.login',
        meta: {
            auth: false
        }
    },
    {
        path: '/auth/register',
        component: RegisterPage,
        name: 'auth.register',
        meta: {
            auth: false
        }
    },
    {
        path: '/setting/general',
        component: GeneralPage,
        name: 'setting.general',
        meta: {
            auth: true
        }
    },
    {
        path: '/setting/picture',
        component: PicturePage,
        name: 'setting.picture',
        meta: {
            auth: true
        }
    },
    {
        path: '/setting/email',
        component: EmailPage,
        name: 'setting.email',
        meta: {
            auth: true
        }
    },
    {
        path: '/setting/password',
        component: PasswordPage,
        name: 'setting.password',
        meta: {
            auth: true
        }
    },
    {
        path: '/setting/privacy',
        component: PrivacyPage,
        name: 'setting.privacy',
        meta: {
            auth: true
        }
    },
    {
        path: '/profile/:username',
        component: ProfilePage,
        name: 'profile',
        meta: {
            auth: true
        }
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(route => route.meta.auth)
    const validToken = hasValidToken()

    if (requiresAuth && !validToken) {
        next({ name: 'auth.login' })
    } else {
        next()
    }
})

export default router
