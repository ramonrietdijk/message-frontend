/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './index.html',
        './src/**/*.{vue,js}'
    ],
    theme: {
        extend: {
            colors: {
                neutral: {
                    850: '#1e1e1e'
                }
            }
        }
    },
    plugins: []
}
